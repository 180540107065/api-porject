package com.vishnu.apiproject.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("movie/top_rated")
    Call<ResponseBody> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<ResponseBody> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
